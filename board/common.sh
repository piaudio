create_missing_dir() {
  test -d "${TARGET_DIR}/$1" || mkdir -p "${TARGET_DIR}/$1"
}

remove_text() {
  echo sed --in-place "s/$2//" $1
  sed --in-place "s/$2//" $1
}

add_tmpfs() {
  create_missing_dir "$1"
  if ! grep -q "$1" "${TARGET_DIR}/etc/fstab"; then
      cat << __EOF__ >> "${TARGET_DIR}/etc/fstab"
tmpfs $1 tmpfs rw 0 0
__EOF__
  fi
}
