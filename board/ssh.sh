# Add dropbear
if [ -d "$BR2_EXTERNAL_PIAUDIO_PATH/local/dropbear" ]; then
    if [ -L ${TARGET_DIR}/etc/dropbear ]; then
        rm ${TARGET_DIR}/etc/dropbear
    fi
    mkdir -p "${TARGET_DIR}"/etc/dropbear/
	cat "$BR2_EXTERNAL_PIAUDIO_PATH/local/dropbear/dropbear_ecdsa_host_key" > "${TARGET_DIR}/etc/dropbear/dropbear_ecdsa_host_key"
	cat "$BR2_EXTERNAL_PIAUDIO_PATH/local/dropbear/dropbear_ed25519_host_key" > "${TARGET_DIR}/etc/dropbear/dropbear_ed25519_host_key"
    mkdir -p "${TARGET_DIR}/root/.ssh"
	cat "$BR2_EXTERNAL_PIAUDIO_PATH/local/dropbear/authorized_keys" > "${TARGET_DIR}/root/.ssh/authorized_keys"
	chmod 600 "${TARGET_DIR}/root/.ssh/authorized_keys"
fi

