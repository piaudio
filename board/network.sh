###########################
#                         #
#  Network configuration  #
#                         #
###########################

# Add default wpa_supplicant.conf if it exists
if [ -f "$BR2_EXTERNAL_PIAUDIO_PATH/wpa_supplicant.conf" ]; then
    create_missing_dir "/etc/wpa_supplicant/"
    cat "$BR2_EXTERNAL_PIAUDIO_PATH/wpa_supplicant.conf" > "${TARGET_DIR}/etc/wpa_supplicant/wpa_supplicant-wlan0.conf"


    cat << __EOF__ > "${TARGET_DIR}/etc/systemd/network/wireless.network"
[Match]
Name=wlan0

[Network]
DHCP=yes
__EOF__

    # Create the link to interface wlan0 directly in the system configuration
    ln -sf /usr/lib/systemd/system/wpa_supplicant@.service "${TARGET_DIR}/usr/lib/systemd/system/multi-user.target.wants/wpa_supplicant@wlan0.service"
    
fi


