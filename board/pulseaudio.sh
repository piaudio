##############################
#                            #
#  Pulseaudio configuration  #
#                            #
##############################

# Create the directory where pulseaudio store its files and put it in tempfs
add_tmpfs "/var/lib/pulse"

if ! grep -qE '^load-module module-native-protocol-tcp' "${TARGET_DIR}/etc/pulse/system.pa"; then
    cat << __EOF__ >> "${TARGET_DIR}/etc/pulse/system.pa"
load-module module-bluetooth-policy
load-module module-bluetooth-discover
load-module module-native-protocol-tcp listen=0.0.0.0 auth-anonymous=1
__EOF__
fi

if ! grep -qE 'avoid-resampling = true' "${TARGET_DIR}/etc/pulse/daemon.conf"; then
    cat << __EOF__ >> "${TARGET_DIR}/etc/pulse/daemon.conf"
avoid-resampling = true
__EOF__
fi
