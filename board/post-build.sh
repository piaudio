#!/bin/sh

set -u
set -e

. "${BR2_EXTERNAL_PIAUDIO_PATH}/board/common.sh"

# Add a console on tty1
if [ -e ${TARGET_DIR}/etc/inittab ]; then
    grep -qE '^tty1::' ${TARGET_DIR}/etc/inittab || \
	sed -i '/GENERIC_SERIAL/a\
tty1::respawn:/sbin/getty -L  tty1 0 vt100 # HDMI console' ${TARGET_DIR}/etc/inittab
fi

# Put the whole system in readonly
sed -ie '/^\/dev\/root/ s/rw 0 1/ro 0 0/' "${TARGET_DIR}/etc/fstab"

. "${BR2_EXTERNAL_PIAUDIO_PATH}/board/network.sh"
. "${BR2_EXTERNAL_PIAUDIO_PATH}/board/pulseaudio.sh"
. "${BR2_EXTERNAL_PIAUDIO_PATH}/board/bluetooth.sh"
. "${BR2_EXTERNAL_PIAUDIO_PATH}/board/mpd.sh"
. "${BR2_EXTERNAL_PIAUDIO_PATH}/board/ssh.sh"

mkdir -p "${TARGET_DIR}"/etc/systemd/system/getty.target.wants
# Add a console on ttyAMA0 and enable auto login
ln -sf /usr/lib/systemd/system/serial-getty@.service "${TARGET_DIR}"/etc/systemd/system/getty.target.wants/serial-getty@ttyGS0.service
sed '/^ExecStart=/ s/-o .-p -- ..u./--skip-login --noclear --noissue --login-options "-f root"/' -i "${TARGET_DIR}"/usr/lib/systemd/system/serial-getty@.service
