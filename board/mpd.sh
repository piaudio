add_tmpfs '/var/lib/mpd'
add_tmpfs "/home/playlists"

cat << __EOF__ > "${TARGET_DIR}/etc/mpd.conf"
# Use a proxy to another instance
music_directory "nfs://192.168.0.4/home/mpd/music"
database {
       plugin "proxy"
       host "192.168.0.4"
       port "6600"
}


# Directory where user-made playlists are stored (RW)
playlist_directory "/home/playlists"

# Log file (RW)
# disable the logs
log_file		"/dev/null"

# Process ID file (RW)
pid_file		"/var/run/mpd.pid"

# State file (RW)
state_file		"/var/lib/mpd/state"

# User id to run the daemon as
#user			"nobody"

# TCP socket binding
bind_to_address		"any"
#bind_to_address	"localhost"

# Unix socket to listen on
bind_to_address		"/var/lib/mpd/socket"

audio_output {
       type            "pulse"
       name            "Pulseaudio"
       server          "127.0.0.1"
}
__EOF__

find "$BR2_EXTERNAL_PIAUDIO_PATH/local/playlists" -name '*.m3u' -type f -print0 | xargs -0 -I '{}' cp {} "${TARGET_DIR}/home/"

cp $BR2_EXTERNAL_PIAUDIO_PATH/local/mpd_playlist.sh "${TARGET_DIR}/home/"

cat << __EOF__ > "${TARGET_DIR}/etc/systemd/system/playlists.service"
[Unit]
Description=Playlists
Before=mpd.target

[Service]
Type=simple
ExecStart=find /home/ -name '*.m3u' -exec cp {} /home/playlists \;

[Install]
WantedBy=multi-user.target
__EOF__

# Create the link to the system configuration
ln -sf /etc/systemd/system/playlists.service "${TARGET_DIR}/usr/lib/systemd/system/multi-user.target.wants/playlists.service"

cat << __EOF__ > "${TARGET_DIR}/etc/systemd/system/rss_playlists.service"
[Unit]
Description=RSS Playlists
Wants=rss_playlists.timer

[Service]
Type=simple
ExecStart=/home/mpd_playlist.sh

[Install]
WantedBy=multi-user.target
__EOF__

cat << __EOF__ > "${TARGET_DIR}/etc/systemd/system/rss_playlists.timer"
[Unit]
Description=Update the RSS playlists every day
Requires=rss_playlists.service

[Timer]
Unit=rss_playlists.service
OnBootSec=1min
OnUnitInactiveSec=1d
RandomizedDelaySec=15m

[Install]
WantedBy=timers.target
__EOF__

ln -sf /etc/systemd/system/rss_playlists.service "${TARGET_DIR}/usr/lib/systemd/system/multi-user.target.wants/rss_playlists.service"

ln -sf /etc/systemd/system/rss_playlists.timer "${TARGET_DIR}/usr/lib/systemd/system/multi-user.target.wants/rss_playlists.timer"
