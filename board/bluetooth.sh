#############################
#                           #
#  Bluetooth configuration  #
#                           #
#############################

create_missing_dir "/etc/bluetooth/"
cat << __EOF__ > "${TARGET_DIR}/etc/bluetooth/main.conf"
[General]

Class = 0x200428
DiscoverableTimeout = 0
PairableTimeout = 0

[Policy]
AutoEnable=true
__EOF__


cat << __EOF__ > "${TARGET_DIR}/etc/systemd/system/bt-agent.service"
[Unit]
Description=Bluetooth Agent
After=bluetooth.service
Requires=bluetooth.service

[Service]
Type=simple
ExecStartPre=bt-adapter --set Discoverable 1
# see https://github.com/RPi-Distro/repo/issues/291
#ExecStart=bt-agent -c NoInputNoOutput
ExecStart=/bin/sh -c '/usr/bin/yes | /usr/bin/bt-agent --capability=DisplayOnly'
# Restart the service each 60s
WatchdogSec=60
RestartSec=60
Restart=always

[Install]
WantedBy=multi-user.target
__EOF__

## The same for the bluetooth, as bluetoothd keep a track for each paired device
create_missing_dir "/var/lib/bluetooth/"
#if ! grep -qE '/var/lib/bluetooth' "${TARGET_DIR}/etc/fstab"; then
#    cat << __EOF__ >> "${TARGET_DIR}/etc/fstab"
#tmpfs /var/lib/bluetooth tmpfs rw 0 0
#__EOF__
#fi
