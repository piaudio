#!/bin/sh
usage() {
    echo "usage: BUILDROOT_DIR=${BUILDROOT_DIR} $0 {boardname} all"
    echo
    echo "with boardname in"
    echo " - raspberrypi0w"
    echo " - raspberrypi3_64"
    echo " - qemu_x86_64"

}

test -z ${BUILDROOT_DIR} && BUILDROOT_DIR="../buildroot-2024.05/"

if [ ! -d ${BUILDROOT_DIR} ]; then
    usage
    echo
    echo "Error: the path to buildroot \"${BUILDROOT_DIR}\" does not exists"
    exit 1
fi

export BOARDNAME=$1

case "$BOARDNAME" in
  raspberrypi0w)
  ;;
  raspberrypi3_64)
  ;;
  qemu_x86_64)
  ;;
  *)
    usage
    echo
    echo "Error: unknown card \"$1\""
    exit 1
  ;;
esac

# Merge custom buildroot configurations
CONFIG_="BR2" KCONFIG_CONFIG="configs/${BOARDNAME}_defconfig" "$BUILDROOT_DIR/support/kconfig/merge_config.sh" -m -r \
    "${BUILDROOT_DIR}/configs/${BOARDNAME}_defconfig" \
    configs/config \
    "configs/$BOARDNAME"
sed "1i ### DO NOT EDIT, this file was automatically generated\n" -i "configs/${BOARDNAME}_defconfig"

## Create full buildroot configuration
BR2_EXTERNAL="$(pwd)" make O="$(pwd)/output/$BOARDNAME" -C "$BUILDROOT_DIR" "${BOARDNAME}_defconfig"

# Build
BR2_EXTERNAL="$(pwd)" make -j8 O="$(pwd)/output/${BOARDNAME}" -C ${BUILDROOT_DIR} "$2"
