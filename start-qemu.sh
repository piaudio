#!/bin/sh
(
BINARIES_DIR="${0%/*}/"
cd ${BINARIES_DIR}

if [ "${1}" = "serial-only" ]; then
    EXTRA_ARGS='-nographic'
else
    EXTRA_ARGS='-serial stdio'
fi

export PATH="output/qemu_x86_64/host/bin:${PATH}"
qemu-system-x86_64 -M pc \
    -kernel output/qemu_x86_64/images/bzImage \
    -drive \
    file=output/qemu_x86_64/images/rootfs.ext2,if=virtio,format=raw \
    -append "rootwait root=/dev/vda console=tty1 console=ttyS0"  \
    -net nic,model=virtio \
    -net user,hostfwd=tcp::6600-:6600,hostfwd=tcp::2222-:22 \
    ${EXTRA_ARGS}
)
