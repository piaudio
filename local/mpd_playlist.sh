#!/bin/sh
# Update the mpd playlist from RSS fields

playlist_directory="/home/playlists"

update_playlist () {
    mpc clear
    mpc load $2
    echo "#EXTM3U" > "${playlist_directory}/Podcast - $1.m3u"
    mpc -f "##EXTINF:0,%title%\n%file%" playlist >> "${playlist_directory}/Podcast - $1.m3u"
    mpc clear
}

case $(mpc status "%state%") in
    stopped|paused)
        update_playlist "Transe fip Express" "https://radiofrance-podcast.net/podcast09/rss_23649.xml"
        update_playlist "Fip Tape" "https://radiofrance-podcast.net/podcast09/rss_24684.xml"
        update_playlist "Live à Fip" "https://radiofrance-podcast.net/podcast09/rss_13416.xml"
        ;;
    *)
esac
