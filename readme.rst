.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

==================================
PIAudio : système pour raspberrypi
==================================

Ce dépôt contient la configuration buildroot permettant de construire un
serveur de son avec un raspberrypi 0 ou raspberrypi 3.

Le système ainsi créé est un système minimaliste avec un serveur pulseaudio
ouvert sur le réseau local, et un point d’accès audio bluetooth.

Installation
============

Pré-requis
----------

Il est nécessaire d’installer `buildroot`_ et créer décompresser l’archive dans
un répertoire parent au dépôt.

.. _buildroot: https://buildroot.org/

Copie du dépôt
--------------

.. code-block:: bash

    git clone https://git.chimrod.com/piaudio.git/

Configuration
-------------

Tout est déjà configuré (ou presque…). Si l’on veut connecter la carte en wifi,
il faut créer un fichier `wpa_supplicant.conf` dans le dépôt avec le nom du
point d’accès et le mot de passe :

::

    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1
    country=FR

    network={
            ssid=""
            psk=""
    }

Construction
============

Lancer la commande suivante pour lancer l’image :


.. code-block:: bash

    # Construire l’image
    ./build.sh raspberrypi0w all

    # Copier sur une clef USB :
    sudo dd if=output/raspberrypi0w/images/sdcard.img of=/dev/XXX bs=4k

L’image du système est d’environ 150Mo, n’importe quel carte SD devrait
fonctionner :)

`raspberrypi0w` peut etre remplacé par `raspberrypi3` dans les commandes
ci-dessus pour cibler cette carte.

Connexion
=========

Le système est « briqué » dans le sens où il n’est pas possible de se connecter
à distance. Par contre, avec un raspberrypi0, il est possible d’ouvrir une
connexion locale en branchant le cable USB sur la prise USB du raspberry pi
(pas la prise d’alimentation), et en le connectant tel quel sur l’ordinateur.

On peut ensuite s’y connecter avec la commande suivante :

.. code-block:: bash

 screen /dev/ttyACM0
